(function ($) {
    $.fn.relativeTime = function (options) {
        var settings = $.extend({
            updating: true,
            interval: 10,
            time_only: false,
            steps: {
                0     : "Just now",
                10    : "10 seconds ago",
                30    : "30 seconds ago",
                "1m"  : "1 minute ago",
                "2m"  : "2 minutes ago",
                "5m"  : "5 minutes ago",
                "10m" : "10 minutes ago",
                "15m" : "more than 15 minutes ago"
            }
        }, options);

        for (key in settings.steps) {
            var mins = key.match(/^([0-9]+)M$/i);
            var hrs = key.match(/^([0-9]+)H$/i);
            var days = key.match(/^([0-9]+)D$/i);

            if (mins != null) {
                mins = parseInt(mins[0]);

                settings.steps[mins * 60] = settings.steps[key];
                delete settings.steps[key];
            } else if (hrs != null) {
                hrs = parseInt(hrs[0]);

                settings.steps[hrs * 3600] = settings.steps[key];
                delete settings.steps[key];
            } else if (days != null) {
                days = parseInt(days[0]);

                settings.steps[days * 3600 * 24] = settings.steps[key];
                delete settings.steps[key];
            }
        }

        var calculateRelativeTime = function (el, initialTime) {
            var now = new Date();
            var diff = now - initialTime;
            var step = null;

            for (key in settings.steps) {
                if (key * 1000 > diff) {
                    break;
                }

                step = settings.steps[key];
            }

            $(el).text(step);
        }

        this.each(function (i, el) {
            var initialTime;

            if (settings.time_only) {
                var curDate = new Date();
                initialTime = new Date(curDate.toDateString() + " " + $(el).text());
            } else {
                initialTime = new Date($(el).text());
            }

            if (settings.updating) {
                calculateRelativeTime(el, initialTime);

                setInterval(function () {
                    calculateRelativeTime(el, initialTime);
                }, settings.interval * 1000);
            } else {
                calculateRelativeTime(el, initialTime);
            }
        });

        return this;
    };
})(jQuery);
